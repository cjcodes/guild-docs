---
title: Code of Conduct
weight: 3
menu: "main"
---

{{<guild-name>}} is dedicated to providing a harassment-free online space for everyone, regardless of gender, gender identity, gender expression, sexual orientation, disability, physical appearance, body size, race, ability, ethnicity, socioeconomic status, or religion (or lack thereof). We do not tolerate harassment of guild mates at any time or in any form.

All online communication should be appropriate for an audience of a wide variety of people of many different backgrounds. Sexual, sexist, racist, or exclusionary language and imagery is never appropriate.

Please be kind to others. Do not insult or put down other guild mates.

Thank you for helping make {{<guild-name>}} a welcoming, friendly place for all.

## Safe space

## Zero-tolerance policy

## Arbitration

## Reporting unacceptable behavior
