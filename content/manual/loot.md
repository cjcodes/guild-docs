---
title: Loot
weight: 20
---

**EPGP and Loot Council.**

## EPGP
[EPGP](http://www.epgpweb.com/help/system) is our primary method of distributing loot.

## Loot Council
Some items are given by loot council. These items include things like:

1. [Thunderfury](https://classic.wowhead.com/item=19019/thunderfury-blessed-blade-of-the-windseeker)
1. [Eye of Sulfuras](https://classic.wowhead.com/item=17204/eye-of-sulfuras)
1. High-level crafting patterns
1. Rare crafting materials
