---
title: Roles and Responsibilities
weight: 10
---

This page outlines the roles and responsibilities within the guild.

## Officers

Our officer roles are very granular and very specific. There are a few reasons for doing this.

1. **Reduce SPOFs.** Distributing the workload results in fewer **S**ingle **P**oints **o**f **F**ailure. While we can all perform many of these functions, distributing the load ensures we have plenty of redundancy within our officer team.
1. **One Decision Maker.** We're trying to avoid a scenario where raiders feel like the officers are of a split mind. To do so, each of these roles carries a "final say" on matters within their scope. For instance, the Loot Master is the decision maker for our loot system.
1. **Opportunities.** With this many officers, there is likely to be turnover. This means that there is a much higher chance of an officer spot opening up for raiders to step up.
1. **Raiding core.** With 8 out of 40 players officers, 20% of the raid core is directly accountable for the success of that raid in one form or another.
1. **Focus.** Instead of trying to be a Jill-of-all-trades, officers can spend time playing their character and filling their main role: a DPS, Tank, or Healer.

### GM

### Scribe

### Strategist: Melee

### Strategist: Heals

### Strategist: Ranged

### Culture and Communications

### Recruitment and Retention

### Loot Master

### PvP Master


## Raiders

### Raider

### Trial

### PvP

### Recruit
