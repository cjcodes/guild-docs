---
date: "2018-11-28T15:14:39+10:00"
title: Home
menu:
  main:
    name: "Home"
weight: 1
name: Toot
---

Whisper is a documentation theme built with Hugo. The design and functionality is intentionally minimal.
